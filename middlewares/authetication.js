const jwt = require('jsonwebtoken');

//Verificar token

const verificaToken = (req, res, next)=>{
    const token = req.get("token");

    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        if(err){
            return res.status(401).json({
                ok: false,
                mensaje: "Error de token",
                err,
            });
        }

        req.usuario = decoded.usuario;

        console.log(req.usuario)

        console.log(token)

        next();
    });
};

module.exports = {
    verificaToken,
};